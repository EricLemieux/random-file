const fs = require('fs-extra');
const getRandomFileFromPath = require('./index');

test('Selects a random file', async () => {
  // Path that does not exist.
  const pathThatDoesNotExist = await getRandomFileFromPath('path-that-does-not-exist');
  expect(pathThatDoesNotExist).toBe(null);

  // Path that is not a directory.
  const pathThatIsNotADirPath = 'package.json';
  const pathThatIsNotADir = await getRandomFileFromPath(pathThatIsNotADirPath);
  expect(pathThatIsNotADir).toBe(pathThatIsNotADirPath);

  // Path is a directory that is empty.
  const exampleDirPath = 'example';
  const exists = await fs.exists(exampleDirPath);

  // Path already exists, so need to clean up first.
  if (exists) {
    fs.remove(exampleDirPath);
  }

  await fs.mkdir(exampleDirPath);
  const pathThatIsEmptyDir = await getRandomFileFromPath(exampleDirPath);
  expect(pathThatIsEmptyDir).toBe(null);

  // Path is a directory that has files.
  const file1Path = `${exampleDirPath}/file1`;
  await fs.writeFile(file1Path, '');
  const pathThatIsFullDir = await getRandomFileFromPath(exampleDirPath);
  expect(pathThatIsFullDir).toBe('file1');

  // Clean up test files.
  fs.remove(exampleDirPath);
});
